import React from "react"
import setupTags from "../utils/setupTags"

const TagsList = ({ recipes }) => {
  const newTags = setupTags(recipes)
  console.log(newTags)
  return (
    <div>
      <p>This is a list of Tags</p>
    </div>
  )
}

export default TagsList
